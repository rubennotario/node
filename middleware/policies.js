const { User } = require("../models/user.model");

module.exports = async function(req, res, next) {
  const user = await User.findById(req.user._id).select("-password");
  if (!user.admin) return res.status(401).send("Access denied. You must be an admin user.");
  next()
};
