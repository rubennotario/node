const mongoose = require("mongoose");
const usersRoute = require("./routes/user.route");
const booksRoute = require("./routes/book.route");
const express = require('express');
const app = express();
const accounts = require('./mocks/accounts');
const PORT = 3000;

mongoose
  .connect("mongodb://localhost/nodejsauth", { useNewUrlParser: true })
  .then(() => console.log("Connected to MongoDB..."))
  .catch(err => console.error("Could not connect to MongoDB..."));

app.use(express.json());

app.use("/api/users", usersRoute);
app.use("/api/books", booksRoute);

app.listen(PORT, () =>
 console.log(`Express server currently running on port ${PORT}`)
);
