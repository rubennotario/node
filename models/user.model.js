const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  admin: Boolean
});

UserSchema.methods.createAuthToken = function() {
  const token = jwt.sign({ _id: this._id, admin: this.admin }, "privateKey"); //get the private key from the config file -> environment variable
  return token;
}

const User = mongoose.model('User', UserSchema);

exports.User = User;
