const request = require('request');

const URL = 'http://localhost:3001/books/'

exports.getAll = function(req, res){
  request(URL, { json: true }, (error, response, body) => {
    if (error) {
      console.error(error)
      return res.json(error)
    }
    if (response.statusCode !== 200) {
      res.status(response.statusCode).json(response.body)
    }else{
      res.json(body)
    }
  })
}

exports.createBook = function(req, res, next){
  request(URL, {
    method: "POST",
    json: req.body
  }, (error, response, body) => {
    if (error) {
      console.error(error)
      return res.json(error)
    }
    if (response.statusCode !== 200) {
      res.status(response.statusCode).json(response.body)
    }else{
      res.json(req.body)
    }
  })
}

exports.updateBook = function(req, res){
  request(URL+req.params.id, {
    method: "PUT",
    json: true
  }, (error, response, body) => {
    if (error) {
      console.error(error)
      return res.json(error)
    }
    if (response.statusCode !== 200) {
      res.status(response.statusCode).json({
       statusCode: response.statusCode,
       body: response.body
      })
    }else{
      res.json(req.body)
    }
  })
}

exports.deleteBook = function(req, res){
  request(URL+req.params.id, {
    method: "DELETE",
    json: true
  }, (error, response, body) => {
    if (error) {
      console.error(error)
      return res.json(error)
    }
    if (response.statusCode !== 200) {
      res.status(response.statusCode).json({
       statusCode: response.statusCode,
       body: response.body
      })
    }else{
      res.json(req.body)
    }
  })
}
