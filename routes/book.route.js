const BooksController = require('../controllers/BooksController');
const log = require("../middleware/console");
const auth = require("../middleware/auth");
const policy = require("../middleware/policies");
const express = require("express");
const router = express.Router();


router.get('/getall', log, BooksController.getAll);
router.post('/createbook/', log, auth, BooksController.createBook);
router.put('/book/:id', log, auth, BooksController.updateBook);
router.delete('/book/:id', log, auth, policy, BooksController.deleteBook);

module.exports = router;
